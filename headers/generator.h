#ifndef __GEN_
#define __GEN_

extern void* GEN_OUT;

void* generator( void* );

void* save( void* );

#endif
