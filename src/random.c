#include "../headers/random.h"

#include <sys/syscall.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

uint64_t random_s( void ){
	uint64_t t;
	syscall( SYS_getrandom, &t, sizeof( t ), 0 );
	return t;
}
