#include "../headers/generator.h"
#include "../headers/random.h"
#include "../headers/control.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

const char* const base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
const int base_len = 62;

void* GEN_OUT;

char is_lock_que;

typedef struct node{
	void* data;
	struct node* last;
} node;

typedef struct queue{
	unsigned long long len;
	node* end;
} queue;

void* queue_pop( queue* que ){
	register void* ret = NULL;
	do{
		sleep( 1 );
	}while( is_lock_que );
	is_lock_que = 1;
	if( ( *que ).len ){
		( *que ).len--;
		ret = ( *que ).end;
		( *que ).end  = ( *( *que ).end ).last;
		( *( node* )ret ).last = NULL;
	}
	is_lock_que = 0;
	return ret;
}
void queue_push( queue* que, void* data ){
	register node** q = &( *que ).end;
	do{
		sleep( 1 );
	}while( is_lock_que );
	is_lock_que = 1;
	if( ( *que ).len ){
		while( *q && ( **q ).last ){
			q = &( **q ).last;
		}
		( **q ).last = malloc( sizeof( node ) );
		memset( ( **q ).last, 0, sizeof( node ) );
		( *( **q ).last ).data = data;
	}else{
		( *que ).end = malloc( sizeof( node ) );
		memset( ( *que ).end, 0, sizeof( node ) );
		( *( *que ).end ).data = data;
	}
	( *que ).len++;
	is_lock_que = 0;
}
void queue_clear( queue* que ){
	do{
		sleep( 1 );
	}while( is_lock_que );
	is_lock_que = 1;
	while( ( *que ).len ){
		free( queue_pop( que ) );
	}
	is_lock_que = 0;
}

void* generator( void* a ){
	register char* ret;
	while( CTRL ){
		ret = malloc( sizeof( *ret ) * 17 );
		memset( ret, 0, 17*sizeof( *ret ) );
		for( register int i = 0; i < 16; i++ ){
			*( ret + i ) = *( base + random_s() % base_len );
		}
		queue_push( a, ret );
	}
	return NULL;
}

void* save( void* que ){
	register long double n = 0.0;
	register void* tmp;
	while( CTRL ){
		if( !is_lock_que && ( *( queue* )que ).len ){
			printf( "Progress: #%.0Lf\r", ++n );
			tmp = queue_pop( que );
			fprintf( GEN_OUT, "%s\n", ( char* )( *( node* )tmp ).data );
			free( tmp );
		}else{
			sleep( 1 );
		}
	}
	queue_clear( que );
	return NULL;
}
