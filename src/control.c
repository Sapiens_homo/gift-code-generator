#include "../headers/control.h"

#include <stdio.h>
#include <stdlib.h>

char CTRL;

void* control( void* arg ){
	char t;
	printf( "Press Enter or ^D while you want to quit......\n" );
	while( scanf( "%c", &t ) == 1 ){
		switch( t ){
			case '\n':
				CTRL = 0;
				break;
			default:
				continue;
		}
		break;
	}
	return NULL;
}
