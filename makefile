$(CC)=gcc -march=native
$(SOFLAG)=-fPIC
$(MKDIR)=mkdir -p build
build/random.o: src/random.c
	$(MKDIR)
	$(CC) $(SOFLAG) -c -o build/random.o src/random.c
build/generator.o: src/generator.c
	$(MKDIR)
	$(CC) $(SOFLAG) -c -o build/generator.o src/generator.c
build/check.o: src/check.c
	$(MKDIR)
	$(CC) $(SOFLAG) -c -o build/check.o src/check.c
build/control.o: src/control.c
	$(MKDIR)
	$(CC) $(SOFLAG) -c -o build/control.o src/control.c
libgift-code.so: build/random.o build/check.o build/generator.o build/control.o

gift-code: main.c libgift-code.so
all: gift-code
	$(CC) -lgift-code -o gift-code main.c
