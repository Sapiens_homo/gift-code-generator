#include "headers/control.h"

#ifdef GENERATE
#include "headers/generator.h"
#endif

#ifdef CHECK
#include "headers/checker.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/sysinfo.h>

int main( int argc, char** argv ){
	const register unsigned nproc = get_nprocs();
	pthread_t* threads;
#ifdef GENERATE
	register signed gen_max_nproc;
	register signed gen_beg;
	queue generate_queue;
#endif

#ifdef CHECK
	register signed chk_max_nproc;
	register signed chk_beg;
	queue check_queue;
#endif

	threads = malloc( sizeof( *threads ) * nproc );

#ifdef GENERATE
	GEN_OUT = fopen( "unchecked", "a" ); 
	for( register int i = gen_beg; i < gen_max_nproc - 1; i++ ){
		pthread_create( threads + i, NULL, generator, &generate_queue );
	}
	pthread_create( threads + gen_beg + gen_max_nproc - 1, NULL, save, &generate_queue );
#endif

#ifdef CHECK
	for( register int i = chk_gen; i < chk_max_nproc - 1; i++ ){
		pthread_create( threads + i, NULL NULL, NULL );
	}
	pthread_create( threads + chk_max_nproc + chk_beg - 1, NULL, NULL, &check_queue );
#endif

	control( NULL );
	free( threads );
	return EXIT_SUCCESS;
}
